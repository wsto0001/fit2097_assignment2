// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "FIT2097_Assignment2.h"
#include "FIT2097_Assignment2GameMode.h"
#include "FIT2097_Assignment2HUD.h"
#include "FIT2097_Assignment2Character.h"
#include "Pickup.h"

AFIT2097_Assignment2GameMode::AFIT2097_Assignment2GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFIT2097_Assignment2HUD::StaticClass();
}
