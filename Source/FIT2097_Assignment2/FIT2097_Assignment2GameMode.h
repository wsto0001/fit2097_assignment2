// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "FIT2097_Assignment2GameMode.generated.h"

UCLASS(minimalapi)
class AFIT2097_Assignment2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFIT2097_Assignment2GameMode();
};



