// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once 
#include "GameFramework/HUD.h"
#include "FIT2097_Assignment2HUD.generated.h"

UCLASS()
class AFIT2097_Assignment2HUD : public AHUD
{
	GENERATED_BODY()

public:
	AFIT2097_Assignment2HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:

};

