// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "InteractableComponent.generated.h"

UENUM()
enum class EInteractable : uint8 {
	m_door				UMETA(DisplayName = "Door"),
	m_lever				UMETA(DisplayName = "Lever"),
	m_switch			UMETA(DisplayName = "Binary Switch")
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FIT2097_ASSIGNMENT2_API UInteractableComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInteractableComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable")
	EInteractable InteractableName;

	EInteractable GetInteractableName();
	
};
