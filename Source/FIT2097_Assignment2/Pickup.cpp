// Fill out your copyright notice in the Description page of Project Settings.

#include "FIT2097_Assignment2.h"
#include "Pickup.h"


// Sets default values
APickup::APickup()
{
 	
}

EPickUpType APickup::GetPickupName()
{
	return PickupName;
}

FText APickup::GetPickUpDisplayName()
{
	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EPickUpType"), true);
	
	FText Name = EnumPtr->GetDisplayNameText();

	if (!EnumPtr) 
		return FText::FromString("Invalid");

	return Name;
}
