// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Pickup.generated.h"

UENUM()
enum class EPickUpType : uint8 {
	m_health		UMETA(DisplayName = "Health"),
	m_key			UMETA(DisplayName = "Key"),
	m_fuse			UMETA(DisplayName = "Torch")
};

UCLASS()
class FIT2097_ASSIGNMENT2_API APickup : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	APickup();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
	EPickUpType PickupName;

	EPickUpType GetPickupName();

	FText GetPickUpDisplayName();
};
